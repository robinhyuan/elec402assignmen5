module csa_tree_GENERIC_REAL(in_0, out_0, out_1);
// synthesis_equation "assign out_0 = ( in_0 * 65 )  ; assign out_1 = 8'b0;"
  input in_0;
  output [7:0] out_0, out_1;
  wire in_0;
  wire [7:0] out_0, out_1;
  assign out_1[0] = 1'b0;
  assign out_1[1] = 1'b0;
  assign out_1[2] = 1'b0;
  assign out_1[3] = 1'b0;
  assign out_1[4] = 1'b0;
  assign out_1[5] = 1'b0;
  assign out_1[6] = 1'b0;
  assign out_1[7] = 1'b0;
  assign out_0[0] = in_0;
  assign out_0[1] = 1'b0;
  assign out_0[2] = 1'b0;
  assign out_0[3] = 1'b0;
  assign out_0[4] = 1'b0;
  assign out_0[5] = 1'b0;
  assign out_0[6] = in_0;
  assign out_0[7] = 1'b0;
endmodule

module csa_tree_GENERIC(in_0, out_0, out_1);
  input in_0;
  output [7:0] out_0, out_1;
  wire in_0;
  wire [7:0] out_0, out_1;
  csa_tree_GENERIC_REAL g1(.in_0 (in_0), .out_0 (out_0), .out_1
       (out_1));
endmodule

module csa_tree_323_GENERIC_REAL(in_0, out_0, out_1);
// synthesis_equation "assign out_0 = ( in_0 * 97 )  ; assign out_1 = 8'b0;"
  input in_0;
  output [7:0] out_0, out_1;
  wire in_0;
  wire [7:0] out_0, out_1;
  assign out_1[0] = 1'b0;
  assign out_1[1] = 1'b0;
  assign out_1[2] = 1'b0;
  assign out_1[3] = 1'b0;
  assign out_1[4] = 1'b0;
  assign out_1[5] = 1'b0;
  assign out_1[6] = 1'b0;
  assign out_1[7] = 1'b0;
  assign out_0[0] = in_0;
  assign out_0[1] = 1'b0;
  assign out_0[2] = 1'b0;
  assign out_0[3] = 1'b0;
  assign out_0[4] = 1'b0;
  assign out_0[5] = in_0;
  assign out_0[6] = in_0;
  assign out_0[7] = 1'b0;
endmodule

module csa_tree_323_GENERIC(in_0, out_0, out_1);
  input in_0;
  output [7:0] out_0, out_1;
  wire in_0;
  wire [7:0] out_0, out_1;
  csa_tree_323_GENERIC_REAL g1(.in_0 (in_0), .out_0 (out_0), .out_1
       (out_1));
endmodule

